Drupal copyright.module README.txt
==============================================================================

Currently there is no HEAD version of the Copyright module. Please use the
latest stable release from http://drupal.org/project/copyright for your
specific Drupal version.

See http://drupal.org/node/17570 as to why this approach was taken.

A new HEAD version of the copyright module will be created once Drupal 6.x
is in code freeze.

Credits / Contact
------------------------------------------------------------------------------

Original author (version 4.5) of this module is Matt Schwartz (matt at
mattschwartz dot net).

Robrecht Jacques (robrechtj at gmail dot com) provided the port to drupal 4.7
and is the current active maintainer.

Best way to contact the authors is to submit a (support/feature/bug) issue at
the projects issue page at http://drupal.org/project/issues/copyright.

